//
//  EditCompanyViewController.swift
//  EmployeeManagementSystem
//
//  Created by fernando simangunsong on 12/08/19.
//  Copyright © 2019 fernando simangunsong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EditCompanyViewController: UIViewController {

    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var contactTextField: UITextField!
    @IBOutlet weak var dobTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    
    private var datepicker:UIDatePicker?
    
    let URL_COMPANY_UPDATE = "http://192.168.1.18:8080/api/v1/company/editCompany"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialData()
        setupDatePicker()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func setInitialData(){
        nameTextField.text = UserDefaults.standard.string(forKey: "name_company")
        emailTextField.text = UserDefaults.standard.string(forKey: "email")
        contactTextField.text = UserDefaults.standard.string(forKey: "contactCompany")
        dobTextField.text = UserDefaults.standard.string(forKey: "dob")
        addressTextField.text = UserDefaults.standard.string(forKey: "address")
        countryTextField.text = UserDefaults.standard.string(forKey: "country")
    }
    
    
    
    @objc func viewTapped(gestureRecog:UITapGestureRecognizer){
        view.endEditing(true)
    }
    
    @objc func dateChanged(datePicker:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dobTextField.text = dateFormatter.string(from: datepicker!.date)
        view.endEditing(true)
    }
    
    func setupDatePicker(){
        datepicker = UIDatePicker()
        
        datepicker?.datePickerMode = .date
        datepicker?.addTarget(self,action:#selector(EditCompanyViewController.dateChanged(datePicker:)),for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(EditCompanyViewController.viewTapped(gestureRecog:)))
        
        view.addGestureRecognizer(tapGesture)
        
        dobTextField.inputView = datepicker
    }
    
    
    @IBAction func goSave(_ sender: Any) {
        let id = UserDefaults.standard.integer(forKey: "idCompany")
        let password = UserDefaults.standard.string(forKey: "password")
        let name = nameTextField.text
        let email = emailTextField.text
        let contact = contactTextField.text
        let dob = dobTextField.text
        let address = addressTextField.text
        let country = countryTextField.text
        
        let parameters: Parameters = [
            "id":id,
            "password": password!,
            "name_company" :name ?? nil!,
            "email" :email ?? nil!,
            "contactCompany" : contact ?? nil!,
            "dateOfBirth" :dob ?? nil!,
            "address" :address ?? nil!,
            "country" : country ?? nil!
        ]
        
        
        Alamofire.request(URL_COMPANY_UPDATE, method: .put, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            
            if let responses = response.result.value {
                let jsonData = responses as! NSDictionary
                print(jsonData)
                
                if(((jsonData.value(forKey: "message"))) as! String == "Update success"){
                    self.displayAlertMessage(alertTitle: "Success", userMessage: "Update Success")
                    UserDefaults.standard.set(name,forKey: "name_company")
                    UserDefaults.standard.set(email,forKey: "email")
                    UserDefaults.standard.set(contact,forKey: "contactCompany")
                    UserDefaults.standard.set(dob,forKey: "dob")
                    UserDefaults.standard.set(address,forKey: "address")
                    UserDefaults.standard.set(country,forKey: "country")
                    return
                }else{
                    self.displayAlertMessage(alertTitle: "Alert", userMessage: "Action Failed")
                    return
                }
            }
        }
        
        
        
    }
    
    func displayAlertMessage(alertTitle: String, userMessage:String){
        
        let alertController = UIAlertController(title: alertTitle, message:
            userMessage, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,
                                                handler: nil))
        self.present(alertController, animated: true, completion: nil)
        
    }

}
