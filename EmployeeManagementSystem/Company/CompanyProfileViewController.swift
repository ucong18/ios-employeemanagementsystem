//
//  CompanyProfileViewController.swift
//  EmployeeManagementSystem
//
//  Created by fernando simangunsong on 12/08/19.
//  Copyright © 2019 fernando simangunsong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CompanyProfileViewController: UIViewController {

    var employeeArray = [String]()
    var departemenArray = [String]()
    var foto = [String]()
    
    let baseurl = "http://192.168.1.18:8080/api/v1/employee/findMyEmployee/"
    var idCompany:String? = nil
   
    
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var contact: UILabel!
    @IBOutlet weak var since: UILabel!
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var companyLogo: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let status = UserDefaults.standard.bool(forKey: "userlogin")
        // navigate to protected page
        if(status == false){
            self.performSegue(withIdentifier: "loginView", sender: self)
        }else{
            setData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setData(){
        self.name?.text = UserDefaults.standard.string(forKey: "name_company")
        self.email?.text = UserDefaults.standard.string(forKey: "email")
        self.contact?.text = UserDefaults.standard.string(forKey: "contactCompany")
        self.since?.text = UserDefaults.standard.string(forKey: "dob")
        self.address?.text = UserDefaults.standard.string(forKey: "address")
        self.country?.text = UserDefaults.standard.string(forKey: "country")
        
        let id = UserDefaults.standard.integer(forKey: "idCompany")
        self.idCompany = String(id)
    }
    
//    @IBAction func goToAddEmployeePage(_ sender: Any) {
//        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "addEmployeePage") as UIViewController
//
//        self.present(viewController, animated: true, completion: nil)
//    }
//
//    @IBAction func goLogout(_ sender: Any) {
//        
//        UserDefaults.standard.set(false, forKey: "userlogin")
//        
//        let domain = Bundle.main.bundleIdentifier!
//        UserDefaults.standard.removePersistentDomain(forName: domain)
//        UserDefaults.standard.synchronize()
//        
//        let status = UserDefaults.standard.bool(forKey: "userlogin")
//        if(status == false){
//            self.performSegue(withIdentifier: "loginView", sender: self)
//        }
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
