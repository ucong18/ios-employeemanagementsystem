//
//  Departemen.swift
//  EmployeeManagementSystem
//
//  Created by fernando simangunsong on 12/08/19.
//  Copyright © 2019 fernando simangunsong. All rights reserved.
//

import UIKit

class Departemen: NSObject {
    var id: Int?
    var name_department : String?
    var deskripsi: String?
    
    init?(name:String, des:String) {
        
        guard !name.isEmpty else {
            return nil
        }
        
        guard !des.isEmpty else {
            return nil
        }
        
        // Initialize stored properties.
        self.name_department = name
        self.deskripsi = des
    }
    
}
