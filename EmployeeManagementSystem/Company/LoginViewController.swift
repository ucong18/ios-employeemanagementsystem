//
//  LoginViewController.swift
//  EmployeeManagementSystem
//
//  Created by fernando simangunsong on 12/08/19.
//  Copyright © 2019 fernando simangunsong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController {

    @IBOutlet weak var labelUsername: UITextField!
    @IBOutlet weak var labelPassword: UITextField!
    
    let URL_COMPANY_LOGIN = "http://192.168.1.18:8080/api/v1/company/login"
    
    let defaultValues = UserDefaults.standard
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func goLogin(_ sender: Any) {
        
        let email = labelUsername.text
        let password = labelPassword.text
        
        
        if(email!.isEmpty || password!.isEmpty ){
            displayAlertMessage(alertTitle: "Alert", userMessage: "All Fields is Required")
            return
        }
        
        if(password!.count < 6){
            displayAlertMessage(alertTitle: "Alert", userMessage: "Password Length 6")
            return
        }
        
        let parameters: Parameters = [
            "email": email!,
            "password":password!
        ]
        
        
        
        Alamofire.request(URL_COMPANY_LOGIN, method: .post ,parameters: parameters, encoding: URLEncoding(destination: .queryString), headers: nil).responseJSON { response in
            
            if let result = response.result.value {
                let jsonData = result as! NSDictionary
                
                if(((jsonData.value(forKey: "message"))) as! String == "Login success"){
                    
                    UserDefaults.standard.set(true, forKey: "userlogin")
                    
                    
                    let companyData = jsonData.value(forKey: "result") as! NSDictionary
                    
                    let userId = companyData.value(forKey: "id") as? Int
                    let password = companyData.value(forKey: "password") as? String
                    let nameCompany = companyData.value(forKey: "name_company") as? String
                    let email = companyData.value(forKey: "email") as? String
                    let kontak = companyData.value(forKey: "contactCompany") as? String
                    let address = companyData.value(forKey: "address") as? String
                    let country = companyData.value(forKey: "country") as? String
                    let dob = companyData.value(forKey: "dateOfBirth") as? String
                    
                    
                    self.defaultValues.set(userId, forKey: "idCompany")
                    self.defaultValues.set(password, forKey: "password")
                    self.defaultValues.set(nameCompany, forKey: "name_company")
                    self.defaultValues.set(email, forKey: "email")
                    self.defaultValues.set(kontak, forKey: "contactCompany")
                    self.defaultValues.set(address, forKey: "address")
                    self.defaultValues.set(country, forKey: "country")
                    self.defaultValues.set(dob, forKey: "dob")
                    self.defaultValues.synchronize()
                    
                    let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeView") as UIViewController
                    
                    self.present(viewController, animated: true, completion: nil)
                    
                    //                    self.performSegue(withIdentifier: "gotomainpage", sender: self)
                    
                }else{
                    self.displayAlertMessage(alertTitle: "Alert", userMessage: "Login Failed")
                }
            }
            
        }
        
    }
    
    func displayAlertMessage(alertTitle: String, userMessage:String){
        
        let alertController = UIAlertController(title: alertTitle, message:
            userMessage, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,
                                                handler: nil))
        self.present(alertController, animated: true, completion: viewDidLoad)
        
    }
    
    @IBAction func goToRegisterView(_ sender: Any) {
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "registerView") as UIViewController
        //
        self.present(viewController, animated: true, completion: nil)
    }
}
