//
//  EmployeeTableViewCell.swift
//  EmployeeManagementSystem
//
//  Created by fernando simangunsong on 12/08/19.
//  Copyright © 2019 fernando simangunsong. All rights reserved.
//

import UIKit

class EmployeeTableViewCell: UITableViewCell {

    @IBOutlet weak var foto: UIImageView!
    @IBOutlet weak var nama: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var departemen: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(_ url: URL){
        getDataFromUrl(url: url) { (data, response, error) in
            DispatchQueue.main.async { () -> Void in
                guard let data = data, error == nil
                    else {
                        return
                }
                self.foto.image = UIImage(data: data)
            }
        }
    }
}
