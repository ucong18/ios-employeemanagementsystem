//
//  EditEmployeeViewController.swift
//  EmployeeManagementSystem
//
//  Created by fernando simangunsong on 12/08/19.
//  Copyright © 2019 fernando simangunsong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class EditEmployeeViewController: UIViewController {

    @IBOutlet weak var namaTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var foto: UIImageView!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var contactTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var dobTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var posittionTextField: UITextField!
    @IBOutlet weak var cvTextField: UITextField!
    @IBOutlet weak var departemenTextField: UITextField!
    
    
    private var datepicker:UIDatePicker?
    
    let URL_COMPANY_UPDATE = "http://192.168.1.18:8080/api/v1/employee/edit"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitialData()
        setupDatePicker()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func setInitialData(){
        namaTextField.text = UserDefaults.standard.string(forKey: "nameEmployee")
        emailTextField.text = UserDefaults.standard.string(forKey: "emailEmployee")
        contactTextField.text = UserDefaults.standard.string(forKey: "company")
        dobTextField.text = UserDefaults.standard.string(forKey: "dob")
        addressTextField.text = UserDefaults.standard.string(forKey: "addressEmployee")
        countryTextField.text = UserDefaults.standard.string(forKey: "countryEmployee")
    }
    
    
    
    @objc func viewTapped(gestureRecog:UITapGestureRecognizer){
        view.endEditing(true)
    }
    
    @objc func dateChanged(datePicker:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dobTextField.text = dateFormatter.string(from: datepicker!.date)
        view.endEditing(true)
    }
    
    func setupDatePicker(){
        datepicker = UIDatePicker()
        
        datepicker?.datePickerMode = .date
        datepicker?.addTarget(self,action:#selector(EditCompanyViewController.dateChanged(datePicker:)),for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(EditCompanyViewController.viewTapped(gestureRecog:)))
        
        view.addGestureRecognizer(tapGesture)
        
        dobTextField.inputView = datepicker
    }
    
    
    func displayAlertMessage(alertTitle: String, userMessage:String){
        
        let alertController = UIAlertController(title: alertTitle, message:
            userMessage, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,
                                                handler: nil))
        self.present(alertController, animated: true, completion: nil)
        
    }

    
    @IBAction func save(_ sender: Any) {
        let idCompany = UserDefaults.standard.integer(forKey: "idCompany")

        let nameEmployee = namaTextField.text
        let emailEmployee = emailTextField.text
        let contactEmployee = contactTextField.text
        let dobEmployee = dobTextField.text
        let addressEmployee = addressTextField.text
        let countryEmployee = countryTextField.text
        let posititionEmployee = posittionTextField.text
        let departemenEmployee = departemenTextField.text
        let cvEmployee = cvTextField.text
        
        
        let parameters: Parameters = [
            "first_name" :nameEmployee ?? nil!,
            "email" :emailEmployee ?? nil!,
            "phone_number" : contactEmployee ?? nil!,
            "dob" :dobEmployee ?? nil!,
            "address" :addressEmployee ?? nil!,
            "country" : countryEmployee ?? nil!,
            
            "positition" : posititionEmployee ?? nil!,
            "company" : idCompany ?? nil!,
            "status" : "AKTIF",
            
        ]
        
        
        Alamofire.request(URL_COMPANY_UPDATE, method: .put, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            
            if let responses = response.result.value {
                let jsonData = responses as! NSDictionary
                print(jsonData)
                
                if(((jsonData.value(forKey: "message"))) as! String == "Update success"){
                    self.displayAlertMessage(alertTitle: "Success", userMessage: "Update Success")
                    UserDefaults.standard.set(nameEmployee,forKey: "nameEmployee")
                    UserDefaults.standard.set(emailEmployee,forKey: "emailEmployee")
                    UserDefaults.standard.set(contactEmployee,forKey: "contactEmployee")
                    UserDefaults.standard.set(dobEmployee,forKey: "dobEmployee")
                    UserDefaults.standard.set(addressEmployee,forKey: "addressEmployee")
                    UserDefaults.standard.set(countryEmployee,forKey: "countryEmployee")
                    return
                }else{
                    self.displayAlertMessage(alertTitle: "Alert", userMessage: "Action Failed")
                    return
                }
            }
        }
        
        
    }
    
}
